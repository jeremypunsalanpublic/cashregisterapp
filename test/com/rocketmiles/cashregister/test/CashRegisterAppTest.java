package com.rocketmiles.cashregister.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.rocketmiles.cashregister.entity.CashRegisterEntity;
import com.rocketmiles.cashregister.service.CashRegisterService;
import com.rocketmiles.cashregister.service.CashRegisterServiceImpl;

public class CashRegisterAppTest {

	public void testShow() {
	
		CashRegisterService service = new CashRegisterServiceImpl();
		
		assertEquals("$0 0 0 0 0 0", service.showCashRegister());
		
	}
	

	public void testPut() {
		
		CashRegisterService service = new CashRegisterServiceImpl();
		
		CashRegisterEntity input = new CashRegisterEntity(new BigDecimal(0), 10, 10, 10, 10, 10);
		
		assertEquals("$380 10 10 10 10 10", service.putCashRegister(input));
	}
	

	
	public void testTake() throws Exception {
		
		CashRegisterService service = new CashRegisterServiceImpl();
		CashRegisterEntity input = new CashRegisterEntity(new BigDecimal(0), 10, 10, 10, 10, 10);
		
		System.out.println(service.putCashRegister(input));
		
		CashRegisterEntity take = new CashRegisterEntity(new BigDecimal(0), 1, 1, 1, 1, 1);
		
		assertEquals("$342 9 9 9 9 9", service.takeCashRegister(take));
		
	}
	
	
	public void testChange() throws Exception {
		CashRegisterService service = new CashRegisterServiceImpl();
		CashRegisterEntity input = new CashRegisterEntity(new BigDecimal(0), 10, 10, 10, 10, 10);
		
		System.out.println(service.putCashRegister(input));
		
		assertEquals("0 1 0 0 1", service.changeCashRegister(11));
		
		assertEquals("$369 10 9 10 10 9", service.showCashRegister());
	}
	
	@Test
	public void testApp() throws Exception {
		CashRegisterService service = new CashRegisterServiceImpl();
		
		assertEquals("$0 0 0 0 0 0", service.showCashRegister());
		
		CashRegisterEntity input = new CashRegisterEntity(new BigDecimal(0), 10, 10, 10, 10, 10);
		
		assertEquals("$380 10 10 10 10 10", service.putCashRegister(input));
		
		CashRegisterEntity take = new CashRegisterEntity(new BigDecimal(0), 1, 1, 1, 1, 1);
		
		assertEquals("$342 9 9 9 9 9", service.takeCashRegister(take));
		
		assertEquals("0 1 0 0 1", service.changeCashRegister(11));
		
		assertEquals("$331 9 8 9 9 8", service.showCashRegister());
		
		
	}
	

}
