/**
 * 
 */
package com.rocketmiles.cashregister.entity;

import java.math.BigDecimal;

/**
 * @author User
 *
 */
public class CashRegisterEntity {
	
	private BigDecimal amount;
	private Integer twentyBill;
	private Integer tenBill;
	private Integer fiveBill;
	private Integer twoBill;
	private Integer oneBill;
	
	public CashRegisterEntity() {
		amount = new BigDecimal(0);
		twentyBill = new Integer(0);
		tenBill = new Integer(0);
		fiveBill = new Integer(0);
		twoBill = new Integer(0);
		oneBill = new Integer(0);
	}
	
	public CashRegisterEntity(BigDecimal amount, Integer twentyBill,
			Integer tenBill, Integer fiveBill, Integer twoBill,
			Integer oneBill) {
		
		this.amount = amount;
		this.twentyBill = twentyBill;
		this.tenBill = tenBill;
		this.fiveBill = fiveBill;
		this.twoBill = twoBill;
		this.oneBill = oneBill;
		
	}
	
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getTwentyBill() {
		return twentyBill;
	}
	public void setTwentyBill(Integer twentyBill) {
		this.twentyBill = twentyBill;
	}
	public Integer getTenBill() {
		return tenBill;
	}
	public void setTenBill(Integer tenBill) {
		this.tenBill = tenBill;
	}
	public Integer getFiveBill() {
		return fiveBill;
	}
	public void setFiveBill(Integer fiveBill) {
		this.fiveBill = fiveBill;
	}
	public Integer getTwoBill() {
		return twoBill;
	}
	public void setTwoBill(Integer twoBill) {
		this.twoBill = twoBill;
	}
	public Integer getOneBill() {
		return oneBill;
	}
	public void setOneBill(Integer oneBill) {
		this.oneBill = oneBill;
	}
	
	
	@Override
	public String toString() {
		return "CashRegisterEntity [amount=" + amount + ", twentyBill=" + twentyBill + ", tenBill=" + tenBill
				+ ", fiveBill=" + fiveBill + ", twoBill=" + twoBill + ", oneBill=" + oneBill + "]";
	}
	
	
	
	

}
