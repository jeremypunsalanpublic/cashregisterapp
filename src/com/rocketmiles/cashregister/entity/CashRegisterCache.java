/**
 * 
 */
package com.rocketmiles.cashregister.entity;

/**
 * @author User
 *
 */
public class CashRegisterCache {
	
	private static CashRegisterEntity currentRegisterCache;
	
	static {
		currentRegisterCache = new CashRegisterEntity();
	}
	
	public static CashRegisterEntity getInstance() {
		return currentRegisterCache;
	}
	
	public static void updateCache(CashRegisterEntity entry) {
		currentRegisterCache = entry;
	}

}
