/**
 * 
 */
package com.rocketmiles.cashregister.service;

import java.math.BigDecimal;

import com.rocketmiles.cashregister.entity.CashRegisterEntity;

/**
 * @author User
 *
 */
public interface CashRegisterService {
	
	String showCashRegister();
	
	String putCashRegister(CashRegisterEntity input);
	
	String takeCashRegister(CashRegisterEntity input) throws Exception;
	
	String changeCashRegister(Integer requestAmount) throws Exception;

}
