/**
 * 
 */
package com.rocketmiles.cashregister.service;

import java.math.BigDecimal;

import com.rocketmiles.cashregister.entity.CashRegisterCache;
import com.rocketmiles.cashregister.entity.CashRegisterEntity;

/**
 * @author User
 *
 */
public class CashRegisterServiceImpl implements CashRegisterService {

	
	@Override
	public String showCashRegister() {
		CashRegisterEntity outputEntity = CashRegisterCache.getInstance();
		return outputCashRegister(outputEntity);
	}

	@Override
	public String putCashRegister(CashRegisterEntity input) {
		CashRegisterEntity record = CashRegisterCache.getInstance();
		BigDecimal inputAmount = new BigDecimal(0);
		Integer twenties = null != input.getTwentyBill() ? input.getTwentyBill() : new Integer(0);
		Integer tens = null != input.getTenBill() ? input.getTenBill() : new Integer(0);
		Integer fives = null != input.getFiveBill() ? input.getFiveBill() : new Integer(0);
		Integer twos = null != input.getTwoBill() ? input.getTwoBill() : new Integer(0);
		Integer ones = null != input.getOneBill() ? input.getOneBill() : new Integer(0);
		inputAmount = new BigDecimal(20).multiply(new BigDecimal(twenties))
				.add(new BigDecimal(10).multiply(new BigDecimal(tens)))
				.add(new BigDecimal(5).multiply(new BigDecimal(fives)))
				.add(new BigDecimal(2).multiply(new BigDecimal(twos)))
				.add(new BigDecimal(1).multiply(new BigDecimal(ones)));
		
		record.setAmount(record.getAmount().add(inputAmount));
		record.setTwentyBill(record.getTwentyBill() + twenties);
		record.setTenBill(record.getTenBill() + tens);
		record.setFiveBill(record.getFiveBill() + fives);
		record.setTwoBill(record.getTwoBill() + twos);
		record.setOneBill(record.getOneBill() + ones);
		
		CashRegisterCache.updateCache(record);
		
		return outputCashRegister(record);
	}

	@Override
	public String takeCashRegister(CashRegisterEntity input) throws Exception {
		CashRegisterEntity record = CashRegisterCache.getInstance();
		BigDecimal inputAmount = new BigDecimal(0);
		Integer twenties = null != input.getTwentyBill() ? input.getTwentyBill() : new Integer(0);
		Integer tens = null != input.getTenBill() ? input.getTenBill() : new Integer(0);
		Integer fives = null != input.getFiveBill() ? input.getFiveBill() : new Integer(0);
		Integer twos = null != input.getTwoBill() ? input.getTwoBill() : new Integer(0);
		Integer ones = null != input.getOneBill() ? input.getOneBill() : new Integer(0);
		inputAmount = new BigDecimal(20).multiply(new BigDecimal(twenties))
				.add(new BigDecimal(10).multiply(new BigDecimal(tens)))
				.add(new BigDecimal(5).multiply(new BigDecimal(fives)))
				.add(new BigDecimal(2).multiply(new BigDecimal(twos)))
				.add(new BigDecimal(1).multiply(new BigDecimal(ones)));
		
		if(record.getAmount().compareTo(inputAmount) >= 0) {
			record.setAmount(record.getAmount().subtract(inputAmount));
		} else {
			throw new Exception("Total take amount " + inputAmount.toPlainString() + " is greater than "
					+ "current amount of " + record.getAmount());
		}
		
		if(record.getTwentyBill().compareTo(twenties) >= 0) {
			record.setTwentyBill(record.getTwentyBill() - twenties);
		} else {
			throw new Exception("Total twenties " + twenties + " is greater than "
					+ "current twenties of " + record.getTwentyBill());
		}
		
		if(record.getTenBill().compareTo(tens) >= 0) {
			record.setTenBill(record.getTenBill()- tens);
		} else {
			throw new Exception("Total tens " + tens + " is greater than "
					+ "current tens of " + record.getTenBill());
		}
		
		if(record.getFiveBill().compareTo(fives) >= 0) {
			record.setFiveBill(record.getFiveBill() - fives);
		} else {
			throw new Exception("Total fives " + fives + " is greater than "
					+ "current fives of " + record.getFiveBill());
		}
		
		if(record.getTwoBill().compareTo(twos) >= 0) {
			record.setTwoBill(record.getTwoBill() - twos);
		} else {
			throw new Exception("Total twos " + twos + " is greater than "
					+ "current twos of " + record.getTwoBill());
		}
		
		if(record.getOneBill().compareTo(ones) >= 0) {
			record.setOneBill(record.getOneBill() - ones);
		} else {
			throw new Exception("Total ones " + ones + " is greater than "
					+ "current ones of " + record.getOneBill());
		}
		
		CashRegisterCache.updateCache(record);
		
		return outputCashRegister(record);
	}

	@Override
	public String changeCashRegister(Integer requestAmount) throws Exception{
		
		CashRegisterEntity record = CashRegisterCache.getInstance();
		
		Integer twentyFromChange = new Integer(0);
		Integer tenFromChange = new Integer(0);
		Integer fiveFromChange = new Integer(0);
		Integer twoFromChange = new Integer(0);
		Integer oneFromChange = new Integer(0);
		Integer totalAmt = new Integer(0);
		
		Integer twentyBill = record.getTwentyBill();
		Integer tenBill = record.getTenBill();
		Integer fiveBill = record.getFiveBill();
		Integer twoBill = record.getTwoBill();
		Integer oneBill = record.getOneBill();
		
		twentyFromChange = requestAmount / 20;
		
		if(twentyFromChange > 0) {
			if(twentyBill >= twentyFromChange) {
				totalAmt = twentyFromChange * 20;
				requestAmount = requestAmount - totalAmt;
				twentyBill = twentyBill - twentyFromChange;
			} else {
				twentyFromChange = twentyBill;
				totalAmt = twentyFromChange * 20;
				requestAmount = requestAmount - totalAmt;
				twentyBill = new Integer(0);
			}
		}
		
		totalAmt = new Integer(0);
		
		tenFromChange = requestAmount / 10;
		
		if(tenFromChange > 0) {
			if(tenBill >= tenFromChange) {
				totalAmt = tenFromChange * 10;
				requestAmount = requestAmount - totalAmt;
				tenBill = tenBill - tenFromChange;
			} else {
				tenFromChange = tenBill;
				totalAmt = tenFromChange * 10;
				requestAmount = requestAmount - totalAmt;
				tenBill = new Integer(0);
			}
		}
		
		totalAmt = new Integer(0);
		
		fiveFromChange = requestAmount / 5;
		
		if(fiveFromChange > 0) {
			if(fiveBill >= fiveFromChange) {
				totalAmt = fiveFromChange * 5;
				requestAmount = requestAmount - totalAmt;
				fiveBill = fiveBill - fiveFromChange;
			} else {
				fiveFromChange = fiveBill;
				totalAmt = fiveFromChange * 5;
				requestAmount = requestAmount - totalAmt;
				fiveBill = new Integer(0);
			}
		}
		
		totalAmt = new Integer(0);
		
		twoFromChange = requestAmount / 2;
		
		if(twoFromChange > 0) {
			if(twoBill >= twoFromChange) {
				totalAmt = twoFromChange * 2;
				requestAmount = requestAmount - totalAmt;
				twoBill = twoBill - twoFromChange;
			} else {
				twoFromChange = twoBill;
				totalAmt = twoFromChange * 2;
				requestAmount = requestAmount - totalAmt;
				twoBill = new Integer(0);
			}
		}
		
		totalAmt = new Integer(0);
		
		oneFromChange = requestAmount / 1;
		
		if(oneFromChange > 0) {
			if(oneBill >= oneFromChange) {
				totalAmt = oneFromChange * 1;
				requestAmount = requestAmount - totalAmt;
				oneBill = oneBill - oneFromChange;
			} else {
				oneFromChange = oneBill;
				totalAmt = oneFromChange * 1;
				requestAmount = requestAmount - totalAmt;
				oneBill = new Integer(0);
			}
		}
		
		if(requestAmount == 0) {
			record.setTwentyBill(twentyBill);
			record.setTenBill(tenBill);
			record.setFiveBill(fiveBill);
			record.setTwoBill(twoBill);
			record.setOneBill(oneBill);
			BigDecimal updatedAmt = new BigDecimal(20).multiply(new BigDecimal(record.getTwentyBill()))
					.add(new BigDecimal(10).multiply(new BigDecimal(record.getTenBill())))
					.add(new BigDecimal(5).multiply(new BigDecimal(record.getFiveBill())))
					.add(new BigDecimal(2).multiply(new BigDecimal(record.getTwoBill())))
					.add(new BigDecimal(1).multiply(new BigDecimal(record.getOneBill())));
			record.setAmount(updatedAmt);
			CashRegisterCache.updateCache(record);
			return (twentyFromChange + " " + tenFromChange + " " 
						+ fiveFromChange + " " + twoFromChange + " " + oneFromChange);
		} else {
			throw new Exception("Sorry");
		}
	}
	
	private String outputCashRegister(CashRegisterEntity entity) {
		StringBuilder outputString = new StringBuilder();
		outputString.append("$")
		.append(entity.getAmount().toPlainString())
		.append(" ")
		.append(entity.getTwentyBill().intValue())
		.append(" ")
		.append(entity.getTenBill().intValue())
		.append(" ")
		.append(entity.getFiveBill().intValue())
		.append(" ")
		.append(entity.getTwoBill().intValue())
		.append(" ")
		.append(entity.getOneBill().intValue());
		return outputString.toString();
	}

}
