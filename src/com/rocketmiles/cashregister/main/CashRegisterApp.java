/**
 * 
 */
package com.rocketmiles.cashregister.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.rocketmiles.cashregister.entity.CashRegisterEntity;
import com.rocketmiles.cashregister.service.CashRegisterService;
import com.rocketmiles.cashregister.service.CashRegisterServiceImpl;

/**
 * @author User
 *
 */
public class CashRegisterApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Ready");
        BufferedReader reader = 
                   new BufferedReader(new InputStreamReader(System.in));   
		String input = null;
		CashRegisterService service = new CashRegisterServiceImpl();
		while(true) {
			try {
				input = reader.readLine();
			} catch (IOException e1) {
				System.err.println(e1.getMessage());
			}
			if(null == input || "".equals(input)) {
				System.out.println("Please input.");
				continue;
			} else {
				input = input.trim().toUpperCase();
			}
			if(input.contains("SHOW")) {
				System.out.println(service.showCashRegister());
				
			} else if (input.contains("PUT")
					|| input.contains("TAKE")) {
				String[] inputs = input.split(" ");
				if(inputs.length != 6) {
					System.err.println("Please input 6 parameters ([Put/Take] [Twenties] [Tens] [Fives] [Twos] [Ones]) ");
					continue;
				}
				CashRegisterEntity inputEntity = new CashRegisterEntity();
				inputEntity.setTwentyBill(new Integer(inputs[1]));
				inputEntity.setTenBill(new Integer(inputs[2]));
				inputEntity.setFiveBill(new Integer(inputs[3]));
				inputEntity.setTwoBill(new Integer(inputs[4]));
				inputEntity.setOneBill(new Integer(inputs[5]));
				if(input.contains("PUT")) {
					System.out.println(service.putCashRegister(inputEntity));
				} else {
					try {
						System.out.println(service.takeCashRegister(inputEntity));
					} catch (Exception e) {
						System.err.println(e.getMessage());
					}
				}
			} else if (input.contains("CHANGE")) {
				String[] inputs = input.split(" ");
				if(inputs.length != 2) {
					System.err.println("Please input 2 parameters ([Change] [ChangeAmount]) ");
					continue;
				}
				Integer requestAmount = new Integer(inputs[1]);
				try {
					System.out.println(service.changeCashRegister(requestAmount));
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				
			} else if (input.contains("QUIT")) {
				System.out.println("Bye");
				break;
			} else {
				System.err.println("Invalid input. Try again");
			}
			
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}

}
